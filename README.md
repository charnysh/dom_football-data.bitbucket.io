## Data Source
http://api.football-data.org/

1) API endpoint to retrieve football teams (English Premier League)
http://api.football-data.org/v2/competitions/2021/teams

2) API endpoint to retrieve football team
http://api.football-data.org/v2/teams/{id}

## Requirements:

(+) 1) when the page is loaded in the left part of the page, all teams should be loaded and rendered
    Team list item contains team title and website

(+) 2) when user clicks on some team in the list, team card in the right part of the page should be loaded
    Team card contains team title, address, email, Player cards.
    Player card contains player name, position and nationality

    Also Team card has a Close button in the upper right corner, clicking on which should close the card.

(-+) 3) when user clicks on some Team card, team website should be opened.

(+) 4) create a logger for logging user actions (inheritance)
for instance basic console logger, console logger (with data)

(+) 5) consider using some templating engine, for example, lodash.js

(-) 6) consider using try/catch functionality to process errors

## Changes 25.07

(+) 1) "var" to "let" (simple-logger)

(+) 2) api token, template element's ids can be placed in constants

(+) 3) data attributes are more reasonable to use to store specific data

(+) 4) use custom entities presenting your objects (for instance, Team)

(+) 5) why team title in team list item is "button"? why not a text

(-) 6) add simple styling

(+) 7) team players list no higher than team list

(+) 8) it's not clear what "this" value will be in function 'cleanParentElement'

(+) 9) what's the difference between onclick and addEventListener

(+) 10) SimpleLogger is a child, not a parent

(+) 11) no "" property

(+) 12) how base class method can be invoked?

(+) 13) inheritance setting

(+) 14) consider returning a constructor of the class with static methods (Logger)

(+?) 15) component approach

## Concepts:

1) (+) DOM events, bubbling, capturing, event loop;

2) AJAX, (+) Promise;

3) prototypal inheritance

## Read about:

 - hoisting

 - lexical environment
 
 - scope
 - 

## Deadline:

 - till Friday evening
