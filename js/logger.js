define (
    function(){
        let loggers = {};
        
        function Logger (moduleName) {
            this.name = moduleName;
        };

        Logger.prototype.write = function (level, message) {
            let str = `${level} ${this.name} -- ${message}`;
            console.log(str);
        };

        Logger.prototype.debug = function (message) {
            this.write("DEBUG", message);
        }; 

        Logger.prototype.info = function (message) {
            this.write("INFO", message);
        }; 
        
        Logger.prototype.error = function (message) {
            this.write("ERROR", message);
        };

        Logger.getLogger = function (moduleName) {
            if (!loggers[moduleName]) {
                loggers[moduleName] = new Logger(moduleName);
            }
            return loggers[moduleName]; 
        };

        return {
            Logger: Logger,
        };
    }
);
