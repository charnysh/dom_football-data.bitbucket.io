define (
    ["logger"],
    function (simpleLoggerModule) {
        const Logger = simpleLoggerModule.Logger;
        const loggingLevels = {
            ERROR: 0,
            DEBUG: 1,
            INFO: 2,
        };

        let loggers = {};

        function  PriorityLogger (moduleName, baseLevel=1) {
            Logger.apply(this, arguments);
            this.baseLevel = baseLevel;
        };

        PriorityLogger.prototype = new Logger();

        PriorityLogger.prototype.write = function (level, message) {
            if (this.baseLevel >= loggingLevels[level]) {
                Logger.prototype.write.call(this, level, message);
            }
        };

        PriorityLogger.getLogger = function (moduleName, baseLevel=1) {
            if (!loggers[moduleName]) {
                loggers[moduleName] = new PriorityLogger(moduleName, baseLevel);
            }
            return loggers[moduleName]; 
        };

        return {
            PriorityLogger: PriorityLogger,
            LoggingLevels: loggingLevels,
        };
    }
);