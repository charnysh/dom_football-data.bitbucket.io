define(
    ["lodash"],
    function (_) {

        function TeamListItemComponent(specs, container, onClickCallback) {
            this.container = container;
            this.name = specs.name;
            this.id = specs.id;
            this.website = specs.website;
            this.onClickCallback = () => {
                    onClickCallback(this); 
                };
        }

        TeamListItemComponent.prototype.unSelect = function () {
            let element = document.querySelector(`li[data-teamid='${this.id}'`);
            element.classList.remove("active");
        };

        TeamListItemComponent.prototype.select = function () {
            let element = document.querySelector(`li[data-teamid='${this.id}'`);
            element.classList.add("active");
        };

        TeamListItemComponent.prototype.render = function () {
            let li = document.createElement("li");
            let  template = document.getElementById("teamListItemTemplate").innerHTML;
            li.innerHTML = _.template(template)({"team": this});
            let aTags = li.getElementsByTagName("a");
            for (var a of aTags) {
                a.addEventListener("click", function (event) {
                    event.preventDefault();
                    event.stopPropagation();
                    window.open(event.target.href, "_blank");
                });
            }
            li.setAttribute("data-teamid", this.id);
            li.addEventListener("click", this.onClickCallback);
            this.container.appendChild(li);
        };

        return TeamListItemComponent;
    }
);