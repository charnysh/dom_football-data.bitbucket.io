require(
    ["loaders", "teamListItemComponent", "teamComponent", "priorityLogger", "lodash"],
    function (loaders, TeamListItemComponent, TeamComponent, loggerModule, _) {
        //entry point to app

        let logger = loggerModule.PriorityLogger.getLogger("app", loggerModule.LoggingLevels.INFO);
        let teamListitemComponents = [];
        let currentActiveTeamListComponent = null;
        const teamsList = document.getElementById("teamsList");
        const teamCard = document.getElementById("team-card");

        const onCardClose = function () {
            currentActiveTeamListComponent.unSelect();
        };

        const onTeamClick = function (clickedListitemComponent) {
            if (currentActiveTeamListComponent){
                currentActiveTeamListComponent.unSelect();
            }
            currentActiveTeamListComponent = clickedListitemComponent;
            currentActiveTeamListComponent.select();

            document.getElementById("team-card").innerHTML = "";
            logger.debug(`onTeamClick id ${clickedListitemComponent.id}`);
            loaders.getTeam(clickedListitemComponent.id)
            .then(
                function (team){
                    logger.debug(`onTeamClick id ${clickedListitemComponent.id} responced`);
                    let teamComponent = new TeamComponent(team, teamCard, onCardClose);
                    teamComponent.render();
                },
                function (error){
                    alert("Can't load team data. Please wait 1 min and try again");
                    logger.error(`onTeamClick id ${clickedListitemComponent.id} ${error}`);
                }
            );         
        };
        
        const onSearchInput = function (event) {
            let query =  new RegExp("^" + event.target.value,"i");
            logger.debug(`onSearchChange querry=${query}`);
            let result = teamListitemComponents.filter(item => {
                let res = query.test(item.name);
                return res;
            });
            teamsList.innerHTML = "";
            result.forEach(item => {
                item.render(onTeamClick);
            });
        };

        let teamSearch = document.getElementById("teamsSearch");
        teamSearch.addEventListener("input", onSearchInput);
        loaders.getTeams()
        .then(
            function (teams) {
                logger.debug("teamsLoaded");
                for (team of teams) {
                    let item = new TeamListItemComponent(team, teamsList, onTeamClick);
                    item.render();
                    teamListitemComponents.push(item);   
                }
            },
            function (error){
                alert("Can't load data. Please wait 1 min and try again");
                logger.error(error);
            }
        ).catch (function (e) {
            logger.error(e);
        });
    }
);