define(
    function () {
        return function (specs){
            this.name = specs.name;
            this.id = specs.id;
            this.address = specs.address;
            this.email = specs.email;
            this.squad = specs.squad;
            this.website = specs.website;
        };
    }
);