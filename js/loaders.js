define(
    ["priorityLogger", "team", "constants"],
    function (loggerModule, Team, constants) {
        let logger = loggerModule.PriorityLogger.getLogger("loaders");
        
        let teamsListParser = function (jsonData) {
            let data = JSON.parse(jsonData);
            return data.teams;
        };

        let teamParser = function (jsonData) {
            let data = JSON.parse(jsonData);
            return new Team(data);
        };

        let createPromiseRequest = function (url, parser) {
            logger.debug("createPromiseRequest");
            return new Promise( function(resolve, reject) {
                let request = new window.XMLHttpRequest();
                request.open('GET', url);
                request.setRequestHeader("X-Auth-Token", constants.apiToken);
               
                request.onload = function () {
                    let parsed = null;
                    if (this.status === 200) {
                        try {
                            parsed = parser(this.responseText); 
                        } catch (e) {
                            if (e instanceof SyntaxError) {
                                alert("Error. Can't parse server response");                            
                            reject(e)               
                            }
                        }
                        resolve(parsed);
                    } else {
                        logger.error("ERROR in create promise");
                        let error = new Error(this.statusText);
                        error.code = this.status;
                        
                        logger.error(`Promise ${error}`);
                        reject(error);
                    }
                }
                request.onerror = function () {
                    console.log(arguments);
                    reject(new Error("cant load"));
                } 
                request.send();
                });
            };

        return {
            getTeams: function () {
                logger.debug("getTeams");
                return createPromiseRequest(constants.getTeamsURL, teamsListParser);
            },

            getTeam: function (id) {
                logger.debug("getTeam");
                return createPromiseRequest(constants.getTeamURL + id, teamParser);
            },
        };
    }
);