define(
    ["priorityLogger"],
    function (priorityLoggerModule) {
        let logger = priorityLoggerModule.PriorityLogger.getLogger("teamComponent");
        const onCloseClickDefault = function (event) {
            event.stopPropagation();
            logger.debug("cleanParentElement");
            event.target.parentElement.innerHTML = "";
        };

        const onCardClickListenerCreator = function (link) {     
            return function (event) {
                window.open(link, '_blank');
            }
        }

        function TeamComponent (team, container, onCardClose) {
            this.team = team;
            this.container = container;
            this.onClose = onCardClose;
        }
        
        TeamComponent.prototype.render = function () {
            logger.debug("viewTeamTemplate");
            let template = document.getElementById("teamCardTemplate").innerHTML;
            let templateFunction = _.template(template );
            let wrapper = document.createElement("section");
            
            wrapper.innerHTML += templateFunction({"team": this.team});
            wrapper.addEventListener("click",
                onCardClickListenerCreator(this.team.website)
            );

            let aTags = wrapper.getElementsByTagName("a");
            for (var a of aTags) {
                a.addEventListener("click", function (event) {
                    event.stopPropagation();
                });
            }
            
            this.container.appendChild(wrapper);

            let closeButton = document.getElementById("closeButton");
            closeButton.addEventListener("click", onCloseClickDefault);
            closeButton.addEventListener("click", this.onClose);
        };
        return TeamComponent;
    }
);